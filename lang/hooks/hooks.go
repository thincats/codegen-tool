package hooks

import "gitee.com/thincats/codegen-tool/langhook"

var hooksRegistry = map[string]func() langhook.FileHook{}

// RegisterFileHook register fileHook factory
func RegisterFileHook(lang string, hook func() langhook.FileHook) {
	hooksRegistry[lang] = hook
}

// GetFileHook gets fileHook by lang name
func GetFileHook(lang string) (langhook.FileHook, bool) {
	hook, ok := hooksRegistry[lang]
	if !ok {
		return nil, ok
	}
	return hook(), ok
}
