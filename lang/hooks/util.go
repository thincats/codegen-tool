package hooks

import "os/exec"

func checkCommandExist(cmd string) bool {
	_, err := exec.LookPath(cmd)
	return err == nil
}
