package lang

import "fmt"

var (
	// UnkonwLang represents language unknwon
	UnkonwLang = "[[unknown_lang]]"
)

type stringSet map[string]bool

var langExtRegistry = map[string]stringSet{}

// ext -> lang
var extLangRegistry = map[string]string{}

// RegisterLangFileExt register extension for lang
func RegisterLangFileExt(lang string, exts ...string) {
	extMap, ok := langExtRegistry[lang]
	if !ok {
		extMap = make(stringSet)
	}
	for _, ext := range exts {
		if oldLang, ok := extLangRegistry[ext]; ok {
			panic(fmt.Errorf("extension to lang is duplicated: (ext: %s, lang: %s, cur_lang: %s)", ext, oldLang, lang))
		}
		extMap[ext] = true
		extLangRegistry[ext] = lang
	}

	langExtRegistry[lang] = extMap
}

// GetLangByExt get lang by file ext
func GetLangByExt(ext string) (string, bool) {
	lang, ok := extLangRegistry[ext]
	return lang, ok
}
