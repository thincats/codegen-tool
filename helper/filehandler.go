package helper

import (
	"errors"
	"os"

	"gitee.com/thincats/codegen-tool/langhook"
	"go.uber.org/multierr"
)

// FileHandler ...
type FileHandler struct {
	files map[string]*os.File

	fileHook langhook.FileHook
}

// NewFileHandler ...
func NewFileHandler(fileHook langhook.FileHook) *FileHandler {
	return &FileHandler{
		files:    make(map[string]*os.File),
		fileHook: fileHook,
	}
}

// GetFile gets *os.File by path, and cache it
func (h *FileHandler) GetFile(path string) (*os.File, error) {
	f, ok := h.files[path]
	if ok {
		return f, nil
	}

	// default open mode
	mode := os.O_RDWR | os.O_CREATE | os.O_TRUNC

	// stat file to determine if file already exists
	fi, err := os.Stat(path)
	if err == nil && fi.IsDir() {
		return nil, errors.New("filename cannot be directory")
	}

	f, err = os.OpenFile(path, mode, 0666)
	if err != nil {
		return nil, err
	}

	// fi is created for the first time
	if fi == nil {
		err := h.fileHook.OnFileCreated(f)
		if err != nil {
			return nil, err
		}
	}

	err = h.fileHook.OnFileOpened(f)
	if err != nil {
		return nil, err
	}

	h.files[path] = f

	return f, nil
}

// Close close all files
func (h *FileHandler) Close() error {
	var err error
	for name, f := range h.files {
		err = multierr.Combine(
			err,
			h.fileHook.OnFileClosing(f),
			f.Close(),
			h.fileHook.OnFileClosed(name),
		)
	}

	return err
}
