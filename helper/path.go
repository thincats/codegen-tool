package helper

import (
	"os"
	"path/filepath"
)

// Glob return all *.ext in dir recusively, and trim dir path
func Glob(dir string, ext string) ([]string, error) {

	files := []string{}
	err := filepath.Walk(dir, func(p string, f os.FileInfo, err error) error {
		if filepath.Ext(p) == ext {
			bp, err := filepath.Rel(dir, p)
			if err != nil {
				// impossible error
				panic(err)
			}
			files = append(files, bp)
		}
		return nil
	})

	return files, err
}
