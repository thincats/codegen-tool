package helper

import (
	"path"
	"regexp"
	"strconv"
	"strings"

	"gitee.com/thincats/codegen-tool/lang"
	"github.com/pkg/errors"
)

// NameSegment segment for filename
type NameSegment struct {
	DirPath      string
	FileName     string
	TemplateType string
	// 0 means all
	ObjectCount int
	FileExt     string
}

var templateTypeRegex = regexp.MustCompile(`(^[a-zA-Z0-9_]+)(?:\[(\d+|\*)\])?$`)

// ParseNameSegment parse NameSegment from dir/fname
func ParseNameSegment(fpath string) (*NameSegment, error) {

	dir, fname := path.Split(fpath)

	strs := strings.Split(fname, ".")

	if len(strs) < 4 {
		return nil, errors.Errorf("ill-formed: %s", fname)
	}

	extPart := strs[len(strs)-3:]
	namePart := strings.Join(strs[:len(strs)-3], ".")

	for i, v := range extPart {
		extPart[i] = strings.Trim(v, " ")
		if len(extPart[i]) == 0 {
			return nil, errors.Errorf("ill-formed: part should not empty: %s", fname)
		}
	}

	ttypePart := extPart[0]
	res := templateTypeRegex.FindStringSubmatch(ttypePart)

	if res == nil {
		return nil, errors.Errorf("ill-formed: unmatched templateType part: %s", fname)
	}

	objectCount := 0
	ttype := res[1]
	// have a[xxx]
	if len(res) > 2 && len(res[2]) != 0 {
		if res[2] != "*" {
			num, err := strconv.ParseInt(res[2], 10, 32)
			if err != nil {
				return nil, errors.Wrap(err, "ill-formed: not int in templateType part")
			}

			if num == 0 {
				return nil, errors.Errorf("ill-formed: templateType part: object count should not be zero: %s", fname)
			}
			objectCount = int(num)
		}
	}

	ns := &NameSegment{
		DirPath:      dir,
		FileName:     namePart,
		TemplateType: ttype,
		ObjectCount:  objectCount,
		FileExt:      extPart[1],
	}

	return ns, nil
}

// LanguageTplFile language file
type LanguageTplFile struct {
	NS       *NameSegment
	FilePath string
}

// ClassifyLangFiles ...
func ClassifyLangFiles(fpaths []string) (map[string][]LanguageTplFile, error) {

	res := map[string][]LanguageTplFile{}

	for _, fpath := range fpaths {
		ns, err := ParseNameSegment(fpath)
		if err != nil {
			return nil, err
		}

		langID, ok := lang.GetLangByExt(ns.FileExt)
		if !ok {
			langID = lang.UnkonwLang
		}

		res[langID] = append(res[langID], LanguageTplFile{
			NS:       ns,
			FilePath: fpath,
		})
	}

	return res, nil
}
