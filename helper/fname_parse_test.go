package helper

import (
	"reflect"
	"testing"
)

func TestParseNameSegment(t *testing.T) {
	tests := []struct {
		name    string
		fname   string
		want    *NameSegment
		wantErr bool
	}{
		{
			name:  "",
			fname: "a.type.go.tpl",
			want: &NameSegment{
				FileName:     "a",
				TemplateType: "type",
				FileExt:      "go",
			},
		},
		{
			name:    "",
			fname:   "type.go.tpl",
			wantErr: true,
		},
		{
			name:  "",
			fname: "{{ .T.name }}.type.go.tpl",
			want: &NameSegment{
				FileName:     "{{ .T.name }}",
				TemplateType: "type",
				FileExt:      "go",
			},
		},
		{
			name:    "",
			fname:   "a..go.tpl",
			wantErr: true,
		},
		{
			name:  "",
			fname: `a.type[233].go.tpl`,
			want: &NameSegment{
				FileName:     "a",
				TemplateType: "type",
				ObjectCount:  233,
				FileExt:      "go",
			},
		},
		{
			name:  "",
			fname: `a.type[*].go.tpl`,
			want: &NameSegment{
				FileName:     "a",
				TemplateType: "type",
				ObjectCount:  0,
				FileExt:      "go",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ParseNameSegment(tt.fname)
			if (err != nil) != tt.wantErr {
				t.Errorf("ParseNameSegment() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseNameSegment() = %v, want %v", got, tt.want)
			}
		})
	}
}
