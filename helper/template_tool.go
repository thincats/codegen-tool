package helper

import (
	"bytes"
	"io"
	"io/ioutil"
	"path"
	"text/template"

	"github.com/pkg/errors"
)

// Tool helper for
type TemplateTool struct {
	templateDir string
	funcs       template.FuncMap
	tpls        map[string]*template.Template
}

// NewTool new tool for template
func NewTemplateTool(templateDir string, funcMap template.FuncMap) *TemplateTool {
	return &TemplateTool{
		templateDir: templateDir,
		funcs:       funcMap,
		tpls:        map[string]*template.Template{},
	}
}

func (t *TemplateTool) loadTemplate(name string) (*template.Template, error) {
	buf, err := ioutil.ReadFile(path.Join(t.templateDir, name))
	if err != nil {
		return nil, err
	}

	// parse template
	tpl, err := template.New(name).Funcs(t.funcs).Parse(string(buf))
	if err != nil {
		return nil, err
	}
	return tpl, nil
}

func (t *TemplateTool) ExecuteRawString(w io.Writer, rawTpl string, obj interface{}) error {
	tpl, err := template.New("").Funcs(t.funcs).Parse(rawTpl)
	if err != nil {
		return errors.WithStack(err)
	}
	return tpl.Execute(w, obj)
}

func (t *TemplateTool) ExecuteRawStringToString(rawTpl string, obj interface{}) (string, error) {
	buf := bytes.NewBufferString("")
	err := t.ExecuteRawString(buf, rawTpl, obj)
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}

func (t *TemplateTool) Execute(w io.Writer, name string, obj interface{}) error {
	tpl, err := t.loadTemplate(name)
	if err != nil {
		return err
	}
	return tpl.Execute(w, obj)
}

// ExecuteCached executes a specified template in the template set using the supplied
// obj as its parameters and writing the output to w.
func (t *TemplateTool) ExecuteCached(w io.Writer, name string, obj interface{}) error {
	tpl, ok := t.tpls[name]
	if !ok {
		var err error
		// attempt to load and parse the template
		tpl, err = t.loadTemplate(name)
		if err != nil {
			return err
		}
		t.tpls[name] = tpl
	}

	return tpl.Execute(w, obj)
}
