package helper

import (
	"encoding/json"
)

// MustJSONMarshal json.Marshal must
func MustJSONMarshal(v interface{}) string {
	b, err := json.Marshal(v)
	if err != nil {
		panic(err)
	}
	return string(b)
}
