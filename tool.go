package codegen

import (
	"fmt"
	"os"
	"path"
	"reflect"

	"gitee.com/thincats/codegen-tool/helper"
	"gitee.com/thincats/codegen-tool/lang/hooks"
	"github.com/pkg/errors"
	"go.uber.org/multierr"
)

// Tool tool
type Tool struct {
	conf  ToolConfig
	ttool *helper.TemplateTool
	store TypeStore
}

// Run runs tool
func (t *Tool) Run() error {

	bundleConfigs := append([]BundleConfig{}, t.conf.BundleConfig)
	bundleConfigs = append(bundleConfigs, t.conf.Extras...)

	var err error
	for _, conf := range bundleConfigs {
		st := t.newSingleRunTool(conf.Template, conf.Output)
		err = multierr.Append(err, st.Run())
	}

	return err
}

func (t *Tool) newSingleRunTool(templateConf TemplateConfig, outputConf OutputConfig) *singleRunTool {
	return &singleRunTool{
		templateConf: templateConf,
		outputConf:   outputConf,
		ttool:        t.ttool,
		store:        t.store,
	}
}

// singleRunTool run tool once
type singleRunTool struct {
	templateConf TemplateConfig
	outputConf   OutputConfig
	ttool        *helper.TemplateTool
	store        TypeStore
}

// Run execute template
func (st *singleRunTool) Run() error {
	files, err := helper.Glob(st.templateConf.Dir, ".tpl")
	if err != nil {
		return errors.WithStack(err)
	}

	langRelFiles, err := helper.ClassifyLangFiles(files)
	if err != nil {
		return err
	}

	for lang, relFiles := range langRelFiles {
		err := st.executeLangFiles(lang, relFiles)
		if err != nil {
			return err
		}
	}

	return nil
}

// executeLangFiles do execution on files based on language
func (st *singleRunTool) executeLangFiles(lang string, tplFiles []helper.LanguageTplFile) (err error) {
	fileHook, ok := hooks.GetFileHook(lang)
	if !ok {
		return errors.Errorf("no such language hook: %s", lang)
	}

	fileHander := helper.NewFileHandler(fileHook)
	defer func() {
		err = multierr.Append(
			err,
			fileHander.Close(),
		)
	}()

	for _, tplFile := range tplFiles {
		ns := tplFile.NS
		outputDir := path.Join(st.outputConf.Dir, ns.DirPath)

		// Mkdir
		err := os.MkdirAll(outputDir, os.ModePerm)

		if err != nil {
			return errors.WithStack(err)
		}

		iter, err := st.store.GetObjectIterator(ns.TemplateType, ns.ObjectCount)
		if err != nil {
			return errors.WithStack(err)
		}

		err = iter.Iterate(func(val interface{}, idx int) error {
			tctx := TemplateContext{
				T:     val,
				Index: idx,
			}
			outputFname, err := st.ttool.ExecuteRawStringToString(ns.FileName, tctx)
			if err != nil {
				return err
			}

			outputPath := path.Join(outputDir, fmt.Sprintf("%s.%s", outputFname, ns.FileExt))
			outputFile, err := fileHander.GetFile(outputPath)
			if err != nil {
				return err
			}

			err = st.ttool.ExecuteCached(outputFile, tplFile.FilePath, tctx)
			if err != nil {
				return err
			}
			return nil
		})

		if err != nil {
			return err
		}
	}

	return nil
}

// TemplateContext context for FileName template
type TemplateContext struct {
	T     interface{}
	Index int
}

// Head return first element of T if T is slice
// otherwise, return T
func (tc *TemplateContext) Head() interface{} {
	rv := reflect.ValueOf(tc.T)

	if rv.Type().Kind() == reflect.Ptr {
		rv = rv.Elem()
	}

	if rv.Type().Kind() == reflect.Slice {
		return rv.Index(0).Interface()
	}

	return tc.T
}
