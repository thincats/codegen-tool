package codegen

// Iterator iterator
type Iterator interface {
	Iterate(handler func(val interface{}, idx int) error) error
}

// IteratorFuncItem item for IteratorFunc
type IteratorFuncItem struct {
	Valid bool
	Val   interface{}
}

// IteratorFunc func impls Iterator
type IteratorFunc func() func() IteratorFuncItem

// Iterate iterate the Iterator
func (f IteratorFunc) Iterate(handler func(val interface{}, idx int) error) error {
	it := f()
	for idx := 0; ; idx++ {
		p := it()
		if !p.Valid {
			break
		}

		err := handler(p.Val, idx)
		if err != nil {
			return err
		}
	}
	return nil
}
