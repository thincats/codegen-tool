## Intro

基于 go template 的代码生成工具

## Feature

- 制定模板目录，支持多种语言输出

## Usage

see `examples/`

## Appendix 1.模板文件名命名约定

`<dir>/<file_name>.<template_type>.<ext>.tpl`

每个 part 的两端空格将被 trim 掉

- `<dir>`: 可以是任意目录结构，生成的文件将保持相同结构
- `<file_name>` 生成文件名
- `<template_type>` 不同的模板类型，模板生成器将会传入对应的模板对象
- `<ext>` 生成文件名的拓展名

如 `visitor/a.type.go.tpl` -> 将会生成 `visitor/a.go`

`<template_type>`有这样的约定

```ebnf

<template_type> ::= <type_name> (<object_count>)?;

<object_count> ::= '[' (<INTEGER> | '*') ']';

<INTEGER> ::= { num > 1 }

```

example: `a.type[1].go.tpl` 或者 `a.type[*].go.tpl`

当`template_type`可以为数组时，可以显式通过`[]`指定每次执行模板时，需要传入的对象数量，`*`代表全部传递。

生成的文件名将是`<file_name>.<ext>`

`<file_name>`的约定

`<file_name>`支持 go template 语法，提供的 object 格式如下

```go
type TemplateContext struct {
  T <type> // template_type对象, 可能为数组或对象
  Index int // template_type为数组时，生成到当前的数组
}

// Head returns head of T, if T is slice
func (*TemplateContext) Head() interface{}
```

example `{{ .T.name }}.type[1].go.tpl`，将会生成

```sh
Expression.go
ConstExpr.go
...
```
