package codegen

// ToolConfig config for Tool
type ToolConfig struct {
	BundleConfig `mapstructure:",squash"`

	Extras []BundleConfig
}

// BundleConfig ...
type BundleConfig struct {
	Output   OutputConfig
	Template TemplateConfig
}

// OutputConfig config for output
type OutputConfig struct {
	Dir string `cli:"usage=output dir" validate:"required"`
}

// TemplateConfig config for template
type TemplateConfig struct {
	Dir string `cli:"usage=template dir" validate:"required"`
}
