package codegen

// TypeStore contains type for template context
type TypeStore interface {
	// GetObjectIterator get iterator for object
	GetObjectIterator(templateType string, unit int) (Iterator, error)
}
