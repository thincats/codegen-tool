module gitee.com/thincats/codegen-tool

go 1.16

require (
	github.com/pkg/errors v0.9.1
	go.uber.org/multierr v1.6.0
)
