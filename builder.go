package codegen

import (
	"text/template"

	"gitee.com/thincats/codegen-tool/helper"
	"github.com/pkg/errors"
)

// StoreLoader load store lazily
type StoreLoader interface {
	LoadStore() (TypeStore, error)
}

// StoreLoaderFunc ...
type StoreLoaderFunc func() (TypeStore, error)

// LoadStore ...
func (l StoreLoaderFunc) LoadStore() (TypeStore, error) {
	return l()
}

// FuncsLoader load funcMap lazily
type FuncsLoader interface {
	LoadFuncs() (template.FuncMap, error)
}

// FuncsLoaderFunc ...
type FuncsLoaderFunc func() (template.FuncMap, error)

// LoadFuncs ...
func (f FuncsLoaderFunc) LoadFuncs() (template.FuncMap, error) {
	return f()
}

// ToolBuilder for Driver
type ToolBuilder struct {
	conf        ToolConfig
	storeLoader StoreLoader
	funcsLoader FuncsLoader
}

// NewToolBuilder new ToolBuilder
func NewToolBuilder() *ToolBuilder {
	return &ToolBuilder{}
}

// WithStoreLoader store loader
func (b *ToolBuilder) WithStoreLoader(loader StoreLoader) *ToolBuilder {
	b.storeLoader = loader
	return b
}

// WithFuncsLoader funcs loader
func (b *ToolBuilder) WithFuncsLoader(loader FuncsLoader) *ToolBuilder {
	b.funcsLoader = loader
	return b
}

// WithConfig builder with config
func (b *ToolBuilder) WithConfig(conf ToolConfig) *ToolBuilder {
	b.conf = conf
	return b
}

// Build builds Driver
func (b *ToolBuilder) Build() (*Tool, error) {

	err := b.checkConfig()
	if err != nil {
		return nil, err
	}

	err = b.checkArgs()
	if err != nil {
		return nil, err
	}

	store, err := b.storeLoader.LoadStore()
	if err != nil {
		return nil, errors.Wrap(err, "build")
	}

	funcs, err := b.funcsLoader.LoadFuncs()
	if err != nil {
		return nil, errors.Wrap(err, "build")
	}

	d := &Tool{
		conf:  b.conf,
		ttool: helper.NewTemplateTool(b.conf.Template.Dir, funcs),
		store: store,
	}

	return d, nil
}

var errNil = errors.New("arg is nil")

func (b *ToolBuilder) checkConfig() error {
	return nil
}

func (b *ToolBuilder) checkArgs() error {
	if b.funcsLoader == nil {
		return errors.WithMessage(errNil, "funcsLoader")
	}

	if b.storeLoader == nil {
		return errors.WithMessage(errNil, "storeLoader")
	}

	return nil
}
