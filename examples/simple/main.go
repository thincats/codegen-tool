package main

import (
	"text/template"

	"gitee.com/thincats/codegen-tool"
	"gitee.com/thincats/codegen-tool/examples/simple/simple"
)

func panicIfErr(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	builder := codegen.NewToolBuilder()

	tool, err := builder.
		WithConfig(
			codegen.ToolConfig{
				BundleConfig: codegen.BundleConfig{
					Output: codegen.OutputConfig{
						Dir: "./out",
					},
					Template: codegen.TemplateConfig{
						Dir: "./templates",
					},
				},
			},
		).
		WithFuncsLoader(codegen.FuncsLoaderFunc(func() (template.FuncMap, error) {
			return template.FuncMap{}, nil
		})).
		WithStoreLoader(codegen.StoreLoaderFunc(func() (codegen.TypeStore, error) {
			return &simple.TypeStore{}, nil
		})).
		Build()
	panicIfErr(err)

	err = tool.Run()
	panicIfErr(err)
}
