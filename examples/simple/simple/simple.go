package simple

import (
	"fmt"

	"gitee.com/thincats/codegen-tool"
)

var _ codegen.TypeStore = &TypeStore{}

// TypeStore type store
type TypeStore struct{}

// GetObjectIterator get iterator for object
func (s *TypeStore) GetObjectIterator(templateType string, unit int) (codegen.Iterator, error) {
	switch templateType {
	case "type":
		return codegen.IteratorFunc(func() func() codegen.IteratorFuncItem {
			valid := true
			return func() codegen.IteratorFuncItem {
				if valid {
					defer func() { valid = false }()
				}
				return codegen.IteratorFuncItem{
					Valid: valid,
					Val:   struct{ Msg string }{"Hello, World"},
				}
			}
		}), nil
	}
	return nil, fmt.Errorf("unknown template type: %s", templateType)
}
