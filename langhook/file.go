package langhook

import "os"

// FileHook hook for file
type FileHook interface {
	// OnFileCreated when file first created in filesystem (not exist previously)
	OnFileCreated(f *os.File) error
	// OnFileOpened when file first opened in program
	OnFileOpened(f *os.File) error
	// OnFileClosing when file is closing
	OnFileClosing(f *os.File) error
	// OnFileClosed when file is closed
	OnFileClosed(path string) error
}

// BaseFileHook default behaviour for FileHook
type BaseFileHook struct{}

// OnFileCreated when file first created in filesystem (not exist previously)
func (b *BaseFileHook) OnFileCreated(f *os.File) error {
	return nil
}

// OnFileOpened when file first opened in program
func (b *BaseFileHook) OnFileOpened(f *os.File) error {
	return nil
}

// OnFileClosing when file is closing
func (b *BaseFileHook) OnFileClosing(f *os.File) error {
	return nil
}

// OnFileClosed when file is closed
func (b *BaseFileHook) OnFileClosed(path string) error {
	return nil
}
